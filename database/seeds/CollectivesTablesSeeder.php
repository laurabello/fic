<?php

use App\User;
use App\Models\Collective;
use Illuminate\Database\Seeder;

class CollectivesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        // Factory to create 50 false Collective
        factory(\App\Models\Collective::class, 50)->create();

        for ($i=0; $i < 100; $i++) {
            $collective = Collective::all()->random(1)->first();
            $user = User::all()->random(1)->first();
            if (!$collective->members->contains($user)) {
                $collective->members()->attach($user);
            }
        }
    }
}
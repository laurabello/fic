<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [[ 'category' => 'Intergénérationnel', 'picture' => 'uploads/category/intergenerationnel.svg'],[ 'category' =>  'Culture','picture' => 'uploads/category/culture.svg' ],[ 'category' => 'Social', 'picture' => 'uploads/category/social.svg'],[ 'category' => 'Numérique','picture' => 'uploads/category/numerique.svg'] ,[ 'category' => 'Environnement', 'picture' => 'uploads/category/environnemental.svg'], [  'category' => 'Vie de la cité','picture' => 'uploads/category/vie-cite.svg' ]];

    	foreach ($categories as $category) {
    		DB::table('categories')->insert([
	            'name' => $category['category'],
	            'slug' => Str::slug($category['category'], '-'),
              'description' => '',
	            'picture' =>  $category['picture']
	        ]);
    	}
    }
}

<?php

use Illuminate\Database\Seeder;

class FicConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
        	['name', 'Nom', 'FIC', ''],
        	['url', 'Url', 'http://fic.loc/', ''],
            ['picture', 'Picture', 'uploads/default/logo-fic-white.svg', ''],
            ['address1', 'Adresse', '12 rue de Bruxelle', ''],
            ['address2', 'Suite', 'La CCI', ''],
            ['zipcode', 'Code Postal', '12000', ''],
            ['city', 'Ville', 'Rodez', ''],
            ['phone', 'Téléphone', '0606060607', ''],
            ['mail', 'Email', 'mail@mail.fr', ''],
            ['longitude', 'Longitude', '21234', ''],
            ['latitude', 'Latitude', '23452323', ''],
            ['googleMaps', 'Google Maps', 'maps maps maps', ''],
            ['socialize', 'Facebook', 'https://www.facebook.com/mjc.onet/', 'facebook'],
            ['socialize', 'Twitter', 'https://twitter.com/mjconet ', 'twitter'],
            ['socialize', 'Instagram', 'https://www.instagram.com/mjc.onet12', 'instagram'],

        ];

    	foreach ($configs as $config) {
    		DB::table('fic_configs')->insert([
	            'option' => $config[0],
	            'option_sub' => $config[1],
	            'value' => $config[2],
	            'value_sub' => $config[3]
	        ]);
    	}
    }
}

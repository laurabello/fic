<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
        // Factory to create 50 false User
        factory(\App\User::class, 50)->create();
    }
}

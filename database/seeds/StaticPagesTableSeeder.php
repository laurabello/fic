<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class StaticPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
          ["title" => "La fic", 
          "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue vitae metus ac venenatis. Donec euismod non ligula in pulvinar. Proin ex ipsum, finibus vitae mi in, bibendum volutpat ligula.", 
          "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue vitae metus ac venenatis. Donec euismod non ligula in pulvinar. Proin ex ipsum, finibus vitae mi in, bibendum volutpat ligula.",
          "picture" => "uploads/default/fic.jpg"
          ],
          ["title" => "La charte", 
          "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue vitae metus ac venenatis. Donec euismod non ligula in pulvinar. Proin ex ipsum, finibus vitae mi in, bibendum volutpat ligula.",
          "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue vitae metus ac venenatis. Donec euismod non ligula in pulvinar. Proin ex ipsum, finibus vitae mi in, bibendum volutpat ligula.",
          "picture" => "uploads/default/people-charte.jpg"
          ],
          ["title" => "Mentions légales", 
          "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue vitae metus ac venenatis. Donec euismod non ligula in pulvinar. Proin ex ipsum, finibus vitae mi in, bibendum volutpat ligula.",
          "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue vitae metus ac venenatis. Donec euismod non ligula in pulvinar. Proin ex ipsum, finibus vitae mi in, bibendum volutpat ligula.",
          "picture" => "uploads/default/mentions.jpg"
          ],
        ];

        foreach ($pages as $page) {
            DB::table("static_pages")->insert([
                "title" => $page["title"],
                "slug" => Str::slug($page["title"], '-'),
                "description" => $page["description"],
                "content" => $page["content"],
                "picture" => $page["picture"],
                "user_id" => 51
            ]);
        }
    }
}

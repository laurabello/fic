<?php

use App\Enums\UserType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(FicConfigsTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'name' => 'Romain',
            'firstname' => 'Delanoë',
            'email' => 'romain@delanoe.me',
            'phone' => 234578787,
            'password' => bcrypt('mrB348p5AH'),
            'role' => UserType::ADMIN
        ]);

        DB::table('users')->insert([
            'name' => 'Jean',
            'firstname' => 'Eudes',
            'email' => 'jean@eudes.fr',
            'password' => bcrypt('mrB348p5AH'),
            'role' => UserType::MODERATOR
        ]);

        DB::table('users')->insert([
            'name' => 'Serge',
            'firstname' => 'Jeanne',
            'email' => 'serge@jeanne.fr',
            'password' => bcrypt('mrB348p5AH'),
            'role' => UserType::DEFAULT
        ]);

        $this->call(IdeasTableSeeder::class);
        $this->call(CollectivesTableSeeder::class);
        $this->call(InitiativesTableSeeder::class);
        $this->call(StaticPagesTableSeeder::class);
    }
}

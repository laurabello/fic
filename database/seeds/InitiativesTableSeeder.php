<?php

use App\User;
use App\Models\Initiative;
use Illuminate\Database\Seeder;

class InitiativesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        factory(\App\Models\Initiative::class, 50)->create();

        for ($i=0; $i < 100; $i++) {
        	$initiative = Initiative::all()->random(1)->first();
        	$user = User::all()->random(1)->first();
        	if (!$initiative->members->contains($user)) {
        		$initiative->members()->attach($user);
        	}
        }
        
    }
}

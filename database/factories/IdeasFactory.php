<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Idea;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Idea::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'author_id' => $faker->randomElement(\App\User::query()->pluck('id')->toArray()),
        'like_count' => random_int(1, 100000),
        'description' => $faker->paragraph,
        'moderate' => random_int(1, 3),
        'category_id' => $faker->randomElement(\App\Models\Category::query()->pluck('id')->toArray()),
    ];
});
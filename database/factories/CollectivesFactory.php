<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Collective;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Collective::class, function (Faker $faker) {
    return [
    	'name' => $faker->company,
        'url' => $faker->url,
        'address1' => $faker->streetName,
        'address2' => $faker->postcode,
        'city' => $faker->city,
        'description' => $faker->paragraph,
        'values' => $faker->paragraph,
        'user_id' => $faker->randomElement(\App\User::query()->pluck('id')->toArray())
    ];
});

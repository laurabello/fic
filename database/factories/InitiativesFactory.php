<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Initiative;
use App\Models\Collective;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Initiative::class, function (Faker $faker) {

	// Date
	$starts_at = Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '+2 days', $endDate = '+3 week')->getTimeStamp()) ;
	$ends_at= Carbon::createFromFormat('Y-m-d H:i:s', $starts_at)->addHours( $faker->numberBetween( 1, 8 ) );

	// Localisation
	$longitude = $faker->longitude($min = -180, $max = 180);
	$latitude = $faker->latitude($min = -90, $max = 90);

    return [
        'name' => $faker->company,
        'like_count' => random_int(1, 100000),
        'description' => $faker->paragraph,
        'event_start' => $starts_at,
        'event_end' => $ends_at,
        'moderate' => random_int(1, 3),
        'address1' => $faker->streetAddress,
        'address2' => $faker->streetName,
        'zip_code' => $faker->postcode,
        'city' => $faker->city,
        'category_id' => $faker->randomElement(\App\Models\Category::query()->pluck('id')->toArray()),
        'price' => Str::random(10),
        'collective_id' => $faker->randomElement(\App\Models\Collective::query()->pluck('id')->toArray())
    ];
});
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectiveQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('collective_question', function (Blueprint $table) {
        $table->unsignedBigInteger('collective_id');
        $table->unsignedBigInteger('question_id');
        $table->text('response');
        $table->timestamps();

        $table->foreign('collective_id')->references('id')->on('collectives')->onDelete('CASCADE');
        $table->foreign('question_id')->references('id')->on('questions')->onDelete('CASCADE');
        $table->primary(['question_id', 'collective_id']);
      });
    }
    /**About the use of pivot tables
     *https://laraveldaily.com/pivot-tables-and-many-to-many-relationships/
     */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('collective_question');
    }
}

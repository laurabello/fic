<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitiativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('initiatives', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('like_count')->default(0);
            $table->text('description')->nullable();
            $table->tinyInteger('moderate')->default(0);
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('collective_id');
            $table->timestamp('event_start')->nullable();
            $table->timestamp('event_end')->nullable();
            $table->string('price')->nullable();
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('city');
            $table->timestamps();
            $table->foreign('collective_id')->references('id')->on('collectives')->onDelete('CASCADE');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('initiatives');
    }
}

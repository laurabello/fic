<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactNewMail;
use App\Mail\ContactAdminNewMail;
use App\Models\Contact;
use App\User;

class NewContactListener
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
	    //dd($event);

        $mails = User::usersAdminMails();
        foreach ($mails as $mail) {
		  Mail::to($mail['email'])->send(new ContactAdminNewMail($event));
        }
        Mail::to($event->contact->email)->send(new ContactNewMail($event));
    }
}

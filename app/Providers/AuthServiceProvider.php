<?php

namespace App\Providers;

use App\Models\Category;
use App\Policies\CategoryPolicy;
use App\Models\StaticPage;
use App\Policies\StaticPagePolicy;
use App\Models\Moderate;
use App\Policies\ModeratePolicy;
use App\Models\FicConfig;
use App\Policies\FicConfigPolicy;
use App\Models\Contact;
use App\Policies\ContactPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Category::class => CategoryPolicy::class,
        StaticPage::class => StaticPagePolicy::class,
        Moderate::class => ModeratePolicy::class,
        FicConfig::class => FicConfigPolicy::class,
        Contact::class => ContactPolicy::class,
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}

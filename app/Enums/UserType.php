<?php

namespace App\Enums;

class UserType
{
    const DEFAULT   = 1;
    const AUTHOR    = 2;
    const MODERATOR = 3;
    const ADMIN     = 4;

    const ALL = [
        self::DEFAULT   => 'User',
        self::AUTHOR    => 'Author',
        self::MODERATOR => 'Moderator',
        self::ADMIN     => 'Administrator',
    ];

    public static function keys(string $glue = null)
    {
        return $glue ? implode($glue, array_keys(static::ALL)) : array_keys(static::ALL);
    }
}

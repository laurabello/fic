<?php

namespace App\Models;
use App\Models\Category;
use App\Models\Initiative;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Collective extends Model
{

  protected $guarded = [];

  protected $hidden = [
      'created_at', 'updated_at',
  ];

  public function initiatives(): HasMany
  {
    return $this->hasMany(Initiative::class);
  }

  public function initiativesCount()
  {
    return $this->initiatives()->count();
  }

  public function owner(): HasOne
  {
    return $this->hasOne(User::class, 'id', 'user_id');
  }

  public function members(): BelongsToMany
  {
    return $this->belongsToMany(User::class)->withPivot('banned')->withTimestamps();
  }
}

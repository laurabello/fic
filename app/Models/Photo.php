<?php

namespace App\Models;

use App\Models\Initiative;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Photo extends Model
{
    protected $guarded = [];

    public function initiatives(): BelongsToMany
    {
        return $this->belongsToMany(Initiative::class);
    }
}

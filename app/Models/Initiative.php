<?php

namespace App\Models;

use App\Models\Collective;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\User;
use App\Models\Photo;
use App\Models\Category;
use App\Models\Reaction;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Initiative extends Model
{
  protected $guarded = [];

  public function scopeActual($query)
  {
      return $query->where('event_start', '>', now());
  }

  public function collective(): BelongsTo
  {
    return $this->belongsTo(Collective::class);
  }

  public function category(): BelongsTo
	{
		return $this->belongsTo(Category::class);
	}

  public function reactions(): MorphMany
  {
      return $this->morphMany(Reaction::class, 'reactable');
  }

  public function members(): BelongsToMany
  {
    return $this->belongsToMany(User::class)->withTimestamps();
  }

    public function photos(): BelongsToMany
  {
    return $this->belongsToMany(Photo::class);
  }
}


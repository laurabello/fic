<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Moderate extends Model
{
	protected $guarded = [];

    public function moderable(): MorphTo
    {
        return $this->morphTo('moderables');
    }
}

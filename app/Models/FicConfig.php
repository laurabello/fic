<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FicConfig extends Model
{
    protected $guarded = [];

    public static function mainConfig()
    {
    	$config = FicConfig::all();

    	$configs = [];
    	$configs['socialize'] = [];
    	$configs['footer'] = [];

    	foreach ($config as $value) {
    		if ($value->option == "socialize") {
    			$socialize = [];
    			$socialize['name'] = $value->option;
    			$socialize['nom'] = $value->option_sub;
    			$socialize['url'] = $value->value;
    			$socialize['logo'] = $value->value_sub;

    			$configs['socialize'][] = $socialize;
    		} else if ($value->option == "footer") {
				$footer = [];
    			$footer['name'] = $value->option;
    			$footer['nom'] = $value->option_sub;
    			$footer['url'] = $value->value;
    			$footer['logo'] = $value->value_sub;

    			$configs['footer'][] = $footer;
    		}else {
	    		$configs[$value->option]['name'] = $value->option_sub;
	    		$configs[$value->option]['value'] = $value->value;
	    		$configs[$value->option]['value_sub'] = $value->value_sub;
	    		$configs[$value->option]['order_column'] = $value->order_column;	
    		}
    	}
    	//dd($configs);
    	return $configs;
    }
}

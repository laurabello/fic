<?php

namespace App;

use App\Models\StaticPage;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\Initiative;
use App\Models\Idea;
use App\Models\Collective;
use App\Enums\UserType;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'firstname', 'phone', 'email', 'description', 'role', 'password', 'picture',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'banned', 'email_verified_at', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function initial(): string
    {
        return strtoupper($this->firstname[0]);
    }

    public function role(): int
    {
        return (int) $this->role;
    }

    public function isModerator(): bool
    {
        return $this->role() === UserType::MODERATOR;
    }

    public function isAdmin(): bool
    {
        return $this->role() === UserType::ADMIN;
    }

    public function participants(): BelongsToMany
    {
        return $this->belongsToMany(Collective::class)->withPivot(['banned'])->withTimestamps();
    }

    public function idea(): HasMany
    {
        return $this->hasMany(Idea::class);
    }

    public function staticPage(): HasMany
    {
        return $this->hasMany(StaticPage::class);
    }

    public function collectives(): HasMany
    {
        return $this->hasMany(Collective::class);
    }

    public function initiatives(): HasManyThrough
    {
        return $this->hasManyThrough(Collective::class, Initiative::class);
    }

    public static function usersAdminMails()
    {
        $mails = User::where('role', UserType::ADMIN)->get('email')->toArray();
        return $mails;
    }

}

<?php

namespace App\Models;

use App\User;
use App\Models\Reaction;
use App\Models\Moderate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Idea extends Model
{
    protected $guarded = [];


    public function author(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

	public function category(): BelongsTo
	{
		return $this->belongsTo(Category::class);
	}

	public function reactions(): MorphMany
	{
	    return $this->morphMany(Reaction::class, 'reactables');
	}

    public function moderates(): MorphMany
	{
	    return $this->morphMany(Moderate::class, 'moderables');
	}
}

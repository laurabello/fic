<?php

namespace App\Models;

use App\Models\Idea;
use App\Models\Initiative;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
	protected $guarded = [];

	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
	
	/**
	* Get the route key for the model.
	*
	* @return string
	*/
	public function getRouteKeyName()
	{
	    return 'slug';
	}

    public function ideas(): HasMany
	{
		return $this->hasMany(Idea::class);
  	}
  
    public function initiative(): HasMany
	{
		return $this->hasMany(Initiative::class);
	}

}

<?php

namespace App\Policies;

use App\User;
use App\Models\StaticPage;
use Illuminate\Auth\Access\HandlesAuthorization;

class StaticPagePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any static pages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the static page.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StaticPage  $staticPage
     * @return mixed
     */
    public function view(User $user, StaticPage $staticPage)
    {
        //
    }

    /**
     * Determine whether the user can create static pages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the static page.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StaticPage  $staticPage
     * @return mixed
     */
    public function update(User $user, StaticPage $staticPage)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the static page.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StaticPage  $staticPage
     * @return mixed
     */
    public function delete(User $user, StaticPage $staticPage)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the static page.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StaticPage  $staticPage
     * @return mixed
     */
    public function restore(User $user, StaticPage $staticPage)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the static page.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StaticPage  $staticPage
     * @return mixed
     */
    public function forceDelete(User $user, StaticPage $staticPage)
    {
        return $user->isAdmin();
    }
}

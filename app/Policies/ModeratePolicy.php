<?php

namespace App\Policies;

use App\User;
use App\Models\Moderate;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModeratePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any moderates.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin() || $user->isModerator();
    }

    /**
     * Determine whether the user can view the moderate.
     *
     * @param  \App\User  $user
     * @param  \App\Moderate  $moderate
     * @return mixed
     */
    public function view(User $user, Moderate $moderate)
    {
        return $user->isAdmin() || $user->isModerator();
    }

    /**
     * Determine whether the user can create moderates.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isModerator();
    }

    /**
     * Determine whether the user can update the moderate.
     *
     * @param  \App\User  $user
     * @param  \App\Moderate  $moderate
     * @return mixed
     */
    public function update(User $user, Moderate $moderate)
    {
        return $user->isAdmin() || $user->isModerator();
    }

    /**
     * Determine whether the user can delete the moderate.
     *
     * @param  \App\User  $user
     * @param  \App\Moderate  $moderate
     * @return mixed
     */
    public function delete(User $user, Moderate $moderate)
    {
        return $user->isAdmin() || $user->isModerator();
    }

    /**
     * Determine whether the user can restore the moderate.
     *
     * @param  \App\User  $user
     * @param  \App\Moderate  $moderate
     * @return mixed
     */
    public function restore(User $user, Moderate $moderate)
    {
        return $user->isAdmin() || $user->isModerator();
    }

    /**
     * Determine whether the user can permanently delete the moderate.
     *
     * @param  \App\User  $user
     * @param  \App\Moderate  $moderate
     * @return mixed
     */
    public function forceDelete(User $user, Moderate $moderate)
    {
        return $user->isAdmin() || $user->isModerator();
    }
}

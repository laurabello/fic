<?php

namespace App\Policies;

use App\User;
use App\Models\FicConfig;
use Illuminate\Auth\Access\HandlesAuthorization;

class FicConfigPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any fic configs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the fic config.
     *
     * @param  \App\User  $user
     * @param  \App\Models\FicConfig  $ficConfig
     * @return mixed
     */
    public function view(User $user, FicConfig $ficConfig)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create fic configs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the fic config.
     *
     * @param  \App\User  $user
     * @param  \App\Models\FicConfig  $ficConfig
     * @return mixed
     */
    public function update(User $user, FicConfig $ficConfig)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the fic config.
     *
     * @param  \App\User  $user
     * @param  \App\Models\FicConfig  $ficConfig
     * @return mixed
     */
    public function delete(User $user, FicConfig $ficConfig)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the fic config.
     *
     * @param  \App\User  $user
     * @param  \App\Models\FicConfig  $ficConfig
     * @return mixed
     */
    public function restore(User $user, FicConfig $ficConfig)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the fic config.
     *
     * @param  \App\User  $user
     * @param  \App\Models\FicConfig  $ficConfig
     * @return mixed
     */
    public function forceDelete(User $user, FicConfig $ficConfig)
    {
        return $user->isAdmin();
    }
}

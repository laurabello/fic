<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Collective;
use App\Models\Initiative;
use Illuminate\Http\Request;


class CollectiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $collectives = Collective::orderBy('name', 'desc')->get();

      return view('collectives.index', compact('collectives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->authorize('create', Category::class);
        $owner = $request->user()->id;

        //dd($request);
        $collective = Collective::create($this->validateRequest($request, $owner));

        //$this->storeImage($collective);
        $collective->save();

        if ($request->expectsJson()) {
            return response()->json([
              'status' => 'OK',
              'type' => 'collective',
              'data' => $collective
            ]);
        }

        //return redirect()->route('categories.index')->with('message', 'Catégorie créée');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Collective $collectif)
    {
        $collectif->load('initiatives');
        foreach ($collectif->initiatives as $initiative) {
          $initiative->load('category');
        }

        $ajax = $request->expectsJson();

        if ($ajax) {
            return response()->json([
              'status' => 'OK',
              'type' => 'collectif',
              'data' => Collective::find($request->id)
            ]);
        }

        return view('collectives.show', compact('collectif'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\Collective $collective
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collective $collective)
    {

      $owner = $request->user()->id;
      $collective->update($this->validateRequest($request, $owner));
      $collective->save();

      $ajax = $request->expectsJson();

      if ($ajax) {
          return response()->json([
            'status' => 'OK',
            'type' => 'collectif',
            'data' => $collective
          ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Collective $collective)
    {

        $collective->delete();
    }

    private function validateRequest($request, $owner)
    {
        $request['user_id'] = $owner;

        return request()->validate([
            'name' => 'required|string|min:3',
            'url' => 'nullable|url',
            'address1' => 'nullable|string|min:3',
            'address2' => 'nullable|string|min:3',
            'zip_code' => 'nullable|string|min:3',
            'city' => 'nullable|string|min:3',
            'user_id' => 'integer',
            'description' => 'nullable|string|min:3',
            'values' => 'nullable|string|min:3',
            'testimony' => 'nullable|string|min:3',
            'home' => 'nullable|integer'
        ]);
    }

    public function addPicture(Request $request)
    {
        $data = request()->validate([
            'id' => 'required|integer',
            'picture' => 'sometimes|file|image|max:5000',
        ]);

        $collective = Collective::find($data['id']);

        $this->storeImage($collective);

        if ($request->expectsJson()) {
            return response()->json([
              'status' => 'OK',
              'type' => 'collective',
              'data' => $collective
            ]);
        }
    }

    private function storeImage($collective)
    {
        if(request()->has('picture')){
            $collective->update([
                'picture'=> request()->picture->store('uploads/collective', 'public'),
            ]);
        }
    }

    public function showAdmin(Request $request)
    {
      return view('admin.collectives.admin');
    }

    public function refColl(Request $request)
    {
      $user = $request->user()->id;
      $collectives = Collective::where('user_id', '=', $user)->orderBy('created_at', 'desc')->get()->load('initiatives');

      return view('admin.collectives.ref-coll', compact('collectives'));
    }

    public function memberColl(Request $request)
    {
        $collectives = User::find($request->user()->id)->load('participants')->participants;
        //dd($collectives);
      //$collectives = User::find($request->user()->id)->collectives;

      return view('admin.collectives.member-coll', compact('collectives'));
    }

    public function deleteImg(Request $request, Collective $collective)
    {
      $collective->picture = NULL;
      $collective->save();
    }

    public function createColl(Request $request)
    {
      $user = $request->user()->id;
      // $collectives = Collective::where('user_id', '=', $user)->get();
      return view('admin.collectives.create');
    }

    public function indexForAdmin()
    {
      $collectives = Collective::withCount('initiatives', 'members')->orderBy('name', 'desc')->get();

      return view('admin.collectives.index', compact('collectives'));
    }

    public function homeColl(Request $request) {

      $collectives = Collective::where('home', '1')->limit(3)->get();

      if($request->expectsJson()) {
        return response()->json([
          'status' => 'OK',
          'type' => 'collective',
          'data' => $collectives
          ]
        );
      }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Idea;
use App\Models\Initiative;
use App\Models\Moderate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ModerateController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth')->except('signal');
    }

	public function index()
	{
		$this->authorize('viewAny', Moderate::class);

		$ideas = Moderate::query()
            ->selectRaw('*')
            ->selectRaw('COUNT(*) AS num_reports')
            ->groupBy('moderables_type', 'moderables_id')
            ->where('moderables_type', 'App\Models\Idea')
            ->orderBy('num_reports', 'desc')
            ->with(['moderable'])
            ->get()
            ->groupBy('moderables_type')
            ->toArray()['App\Models\Idea'];
		    //dd($ideas);

        return view('admin.moderates.index', compact('ideas'));
	}

	public function reject(Moderate $moderate)
	{
		$this->authorize('update', $moderate);
		
		$model = $moderate->moderable;
		$moderates = Moderate::where('moderables_type', get_class($model))->where('moderables_id', $model->id)->get();
		$model->delete();
		foreach ($moderates as $moderate) {
			$moderate->delete();
		}

		return redirect()->route('moderate.index');
	}

	public function valide(Moderate $moderate)
	{
		$this->authorize('update', $moderate);

		$model = $moderate->moderable;
		$moderates = Moderate::where('moderables_type', get_class($model))->where('moderables_id', $model->id)->get();

		foreach ($moderates as $moderate) {
			$moderate->delete();
		}
		return redirect()->route('moderate.index');
	}

    public function signal(Request $request)
    {
    	$data = request()->validate([
            'id' => 'required|integer',
            'type' => 'required|string|min:3',
        ]);

    	$userModerate = '';

    	if ($data['type'] === "idea") {
    		$model = Idea::where('id', $data['id'])->first();
    		if ($request->user() && Moderate::where(['user_id' => $request->user()->id, 'moderables_type' => 'App\Models\Idea', 'moderables_id' => $model->id])->exists()) {
    			$userModerate = 'exist';
    		}
    	} else if ($data['type'] === "initiative") {
    		$model = Initiative::where('id', $data['id'])->first();
    		if ($request->user() && Moderate::where(['user_id' => $request->user()->id, 'moderables_type' => 'App\Models\Initiative', 'moderables_id' => $model->id])->exists()) {
    			$userModerate = 'exist';
    		}
    	}
    	
    	if (!$userModerate){
	    	$moderate = new Moderate;
	    	if ($request->user()) {
	    		$moderate->user_id = $request->user()->id;
	    	}
	        $moderate->moderable()->associate($model);
	        $moderate->save();
	    }

	    return response()->json([
	      'status' => 'OK',
	      'type' => 'moderate',
	    ]);
    }
}

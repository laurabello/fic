<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FicConfig;
use App\Models\Contact;
use App\Events\NewContactEvent;

class ContactController extends Controller
{

    public function index() {
      $contacts = Contact::all();
      return view('admin.messages.index', compact('contacts'));
    }

    public function show() {
      $config = FicConfig::mainConfig();
      return view('page.contact', compact('config'));
    }

    public function store(Request $request) 
    {
        
        $contact = Contact::create(($this->validateRequest($request)));
        
        event(new NewContactEvent($contact));

        $contact->save();

        return redirect()->back()->with('message', 'Votre message a bien été envoyé, merci.');
    }

    public function showMessage(Contact $contact)
    {
      return view('admin.messages.show', compact('contact'));
    }

    private function validateRequest(Request $request) 
    {
        return request()->validate([
            'firstname' => 'required|min:3',
            'lastname' => 'nullable',
            'email' => 'required',
            'phone' => 'nullable',
            'message' => 'required|min:10',
            'title' => 'required|min:3'
        ]);

    }
}

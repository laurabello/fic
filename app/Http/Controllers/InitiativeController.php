<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Initiative;
use App\Models\Category;
use App\Models\Collective;
use Illuminate\Http\Request;

class InitiativeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show', 'homeInitiative', 'initByCat', 'filterInit']);
    }

    /**
     * Display all the resources.
     */
    public function index(Request $request)
    {
        $initiatives = Initiative::actual()->orderBy('created_at', 'desc')->get()->load('category', 'photos');
        $categories = Category::all();

        return view('initiatives.index', compact('initiatives', 'categories'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Initiative $initiative)
    {

        if ($request->expectsJson()) {
            return response()->json([
              'status' => 'OK',
              'type' => 'initiative',
              'data' => Initiative::find($request->id)
            ]);
        }


        $initiative->load('category', 'collective', 'members', 'photos', 'collective.owner');
        
        $similarInits = Initiative::where('category_id', $initiative->category_id)
        ->whereNotIn('id', [$initiative->id])
        ->with('photos')
        ->actual()
        ->orderBy('event_start', 'desc')
        ->limit(3)
        ->get();

        $similarInits->load('category');
        
        return view('initiatives.show', compact('initiative', 'similarInits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $initiative = new Initiative;
        return view('initiatives.create', compact('initiative'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $initiative = Initiative::create($this->validateRequest($request));
        $initiative->photos()->attach($request->pictures);
        
        $initiative->save();
        

        if ($request->expectsJson()) {
            return response()->json([
              'status' => 'OK',
              'type' => 'initiative',
              'data' => $initiative->id
            ]);
        }

        return redirect()->route('initiatives.index')->with('message', 'Initiative créée');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Initiative $initiative)
    {
        return view('initiatives.edit', compact('initiative'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Initiative $initiative)
    {
        $request = request()->validate([
	       'name' => 'required|min:3',
	       'description' => 'nullable'
        ]);

        $initiative->name = $request['name'];
        $initiative->slug = strtolower($request['name']);
        $initiative->description = $request['description'];
        $initiative->save();

        return redirect()->route('initiatives.index')->with('message', 'Initiative mise à jour');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Initiative $initiative)
    {
        $initiative->delete();
        return redirect()->route('initiatives.index')->with('message', 'Initiative supprimée');
    }


    /**
     * Function to retrieve the datas for the 3 next initiatives
     */
    public function homeInitiative(Request $request)
    {
        $ajax = $request->expectsJson();
        $initiatives = Initiative::actual()
        ->with('category', 'photos')
        ->where('event_start', '>', now())
        ->orderBy('event_start')
        ->limit(3)
        ->get();
        if ($ajax) {
            return response()->json([
              'status' => 'OK',
              'type' => 'initiative',
              'data' => $initiatives
            ]);
        }
    }

    public function initByCat(Category $category)
    {
      $initiatives = Initiative::actual()->with('category', 'photos')->where('category_id', '=', $category->id)->get();
      return view('initiatives.initByCat', compact('initiatives', 'category'));
    }

    private function validateRequest($request)
    {
        return request()->validate([
            'name'          => 'required|string|min:3',
            'description'   => 'nullable|string',
            'category_id'   => 'required|integer',
            'event_start'   => 'required|date',
            'event_end'     => 'required|date',
            'price'         => 'nullable|string',
            'localisation'  => 'nullable|string',
            'collective_id' => 'required|integer',
            'id' => 'integer'
        ]);
    }

    public function indexAdmin(Request $request) 
    {
      $user = $request->user();
      $user->load('collectives');
      $categories = Category::all();
      $collectives = Collective::where('user_id', $user->id)->with('initiatives', 'initiatives.photos', 'initiatives.category')->get();

      return view('admin.initiatives.index', compact('collectives', 'user', 'categories'));
    }

    public function joinInitiative(Request $request)
    {
        $data = request()->validate([
            'id'     => 'required|integer',
            'type'   => 'required|string',
            'user'   => 'required|integer',
        ]);

        $user = $request->user();

        if ($data['type'] == 'initiative' && $data['user'] === $user->id) {

            $initiative = Initiative::find($data['id']);

            if (!$initiative->members->contains($user)) {
                $initiative->members()->attach($user);
                $type = 'join';
            } else {
                $initiative->members()->detach($user);
                $type = 'leave';
            }

        } else if ($data['type'] == 'collective' && $data['user'] === $user->id) {
            $collective = Collective::find($data['id']);

            if (!$collective->members->contains($user)) {
                $collective->members()->attach($user);
                $type = 'join';
            } else {
                $collective->members()->detach($user);
                $type = 'leave';
            }
        }

        if ($request->expectsJson()) {
            return response()->json([
              'status' => 'OK',
              'type' => $type,
              'data' => $user
            ]);
        }
    }

    public function updateInit(Request $request) {
      $initiative = Initiative::find($request->id);
      $initiative->update($this->validateRequest($request));
      $initiative->save();

      $ajax = $request->expectsJson();

      if ($ajax) {
          return response()->json([
            'status' => 'OK',
            'type' => 'collectif',
            'data' => $initiative
          ]);
      }
    }

    public function deleteInit(Request $request, Initiative $initiative) {
      
      $initiative->delete();
    }
}

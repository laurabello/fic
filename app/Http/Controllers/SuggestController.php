<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SuggestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $req = $request->segment(2);
        return view('suggest.index', compact('req'));
    }
}

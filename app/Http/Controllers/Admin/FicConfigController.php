<?php

namespace App\Http\Controllers\Admin;

use App\Models\FicConfig;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FicConfigController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
    	$this->authorize('viewAny', FicConfig::class);

    	$ficConfigs = FicConfig::all();

    	return view('admin.ficconfigs.edit', compact('ficConfigs'));
    }

    public function updateHeader(Request $request)
    {
    	$this->authorize('create', FicConfig::class);

    	$configs = ['name', 'picture'];
		
		
 
    		//	dd($request);

    	foreach ($configs as $config) {
    		//dd($config);
    		if ($config == 'picture') {
    		FicConfig::where('option', $config)->first()->update(['value'=> request()->picture->store('uploads/main', 'public')]);
    		} else {
    			FicConfig::where('option', $config)->first()->update(['value' => $request[$config]]);
    		}
    	}

    	return redirect()->route('configuration');
    }

    public function updateFooter(Request $request)
    {
    	$this->authorize('create', FicConfig::class);

    	$configs = ['address1', 'address2', 'zipcode', 'city', 'phone', 'mail', 'longitude', 'latitude', 'googleMaps'];
    	
    	foreach ($configs as $config) {
    		FicConfig::where('option', $config)->first()->update(['value' => $request[$config]]);
    	}

    	return redirect()->route('configuration');
    }

    public function updateSocialize(Request $request)
    {
    	$this->authorize('create', FicConfig::class);

    	$socializes = FicConfig::where('option', 'socialize')->get();
    	//dd($socializes);

    	foreach ($socializes as $socialize) {
    		if ($url = filter_var(htmlentities($request[$socialize->option_sub]), FILTER_VALIDATE_URL)) {
    			FicConfig::where('option', 'socialize')->where('option_sub', $socialize->option_sub)->first()->update(['value' => $url]);
    		}
    		else {
    			// display errors
    		}
    	}
    	

    	$ficConfigs = FicConfig::all();

    	return redirect()->route('configuration');
    }

    public function addSocialize(Request $request)
    {
    	$this->authorize('create', FicConfig::class);

    	$request = request()->validate([
	       'name' => 'required|min:3',
           'url' => 'url'
        ]);

        $ficConfig = new FicConfig;
        $ficConfig->option = "socialize";
        $ficConfig->option_sub = $request['name'];
		$ficConfig->value = $request['url'];
		$ficConfig->value_sub = strtolower($request['name']);
        $ficConfig->save();

        $ficConfigs = FicConfig::all();

    	return redirect()->route('configuration');
    }

    public function deleteSocialize(Request $request, FicConfig $ficConfig)
    {
    	$this->authorize('delete', $ficConfig);

		$ficConfig->delete();

        if ($request->expectsJson()) {
        return response()->json([
          'status' => 'OK',
          'type' => 'socialize',
          'data' => 'delete'
          ]
        );
        }

		return redirect()->route('configuration');
    }

    private function storeImage($picture) {
        if(request()->has('picture')){
            $picture->update([
                'picture'=> request()->picture->store('uploads/main', 'public/'),
            ]);
        }
    }
}

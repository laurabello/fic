<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StaticPage;
use Illuminate\Support\Str;

class StaticPageController extends Controller
{
    public function index(Request $request)
    {
        $staticPages = StaticPage::all();
        
        return view('admin.static-pages.index', compact('staticPages'));
    }

    /**
     * Display the specified resource for admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(StaticPage $staticPage)
    {
      return view('admin.static-pages.show', compact('staticPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$staticPage = new StaticPage;

        return view('admin.static-pages.create', compact('staticPage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', StaticPage::class);

        $staticPage = StaticPage::create($this->validateRequest());

        $staticPage->save();

        return redirect()->route('static-pages.index')->with('message', 'Page créée');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(StaticPage $staticPage)
    {
        return view('admin.static-pages.edit', compact('staticPage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaticPage $staticPage)
    {

        $this->authorize('update', $staticPage);

        $staticPage->update($this->validateRequest($request));

        $staticPage->save();

        return redirect()->route('static-pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        $this->authorize('delete', $staticPage);
        $staticPage->delete();

        return redirect()->route('static-pages.index');
    }

    private function validateRequest($request)
    {
        $request['slug'] = Str::slug($request['title']);

        return request()->validate([
            'title' => 'required|min:3',
            'slug' => 'required|min:3',
            'content' => 'required|min:30',
            'description' => 'nullable',
            'picture' => 'nullable'
        ]);
    }

    /**
     * Display the specified resource for visitors.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showForAll(StaticPage $staticPage)
    {
        return view('page.show', compact('staticPage'));
    }
}

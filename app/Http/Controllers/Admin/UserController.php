<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Collective;
use App\Models\Initiative;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::orderBy('created_at', 'desc')->get();

        return view('admin.profile.index', compact('users'));
    }

    public function update(Request $request, User $user)
    {

        if($request->user()->id == $request->id ) {

            $user = User::find($request->user()->id);

            $user->update($this->validateRequest($request));

            $user->save();

            if ($request->expectsJson()) {
                return response()->json([
                  'status' => 'OK',
                  'type' => 'user',
                  'data' => $user
                ]);
            }
        }


        return view('admin.profile.index', compact('users'));
    }
    
    public function dashboard(Request $request)
    {
        $user = $request->user()->load('collectives');

        return view('admin.profile.dashboard', compact('user'));
    }



    public function addPicture(Request $request)
    {
        $data = request()->validate([
            'id' => 'required|integer',
            'picture' => 'sometimes|file|image|max:5000',
        ]);

        $user = User::find($data['id']);
        $this->storeImage($user);

        if ($request->expectsJson()) {
            return response()->json([
              'status' => 'OK',
              'type' => 'user',
              'data' => $user
            ]);
        }
    }

    private function storeImage($user) {
        if(request()->has('picture')){
            $user->update([
                'picture'=> request()->picture->store('uploads/user', 'public'),
            ]);
        }
    }

    public function showName(Request $request, User $user) {

        if ($request->expectsJson()) {
            return response()->json([
              'status' => 'OK',
              'type' => 'user',
              'data' => [$user->name, $user->initial()]
            ]);
        }
        return view('admin.profile.index');
    }

    public function deletePicture(User $user)
    {
      $user->picture = NULL;
      $user->save();
    }

    public function ban(Request $request)
    {
        $data = request()->validate([
            'id' => 'required|integer',
            'type' => 'required|string',
            'user' => 'required|integer',
        ]);

        $user = User::find($data['id']);
        $owner = $request->user();

        if ($data['type'] == 'collective') {
            $model = Initiative::find($data['id']);
            $ownerTest = $model->collective->user_id;
        } else if ($data['type'] == 'initative') {
            $model = Collective::find($data['id']);
            $ownerTest = $model->user_id;
        }

        if($ownerTest == $data['user'] || $request->user()->isAdmin()) {
            $user->banned = now();
            $user->save();
        }

        if ($request->expectsJson()) {
            return response()->json([
              'status' => 'OK',
              'type' => 'user',
              'data' => $user->banned
            ]);
        }
    }

    private function validateRequest($request)
    {

        return request()->validate([
            'name' => 'required|min:3',
            'firstname' => 'required|min:3',
            'email' => 'required|min:8',
            'phone' => 'nullable|string|min:3',
            'description' => 'nullable|string|min:3',
            'role' => 'integer',
            'banned' => 'nullable|date',
        ]);
 
    }
}

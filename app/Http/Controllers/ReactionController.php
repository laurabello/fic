<?php

namespace App\Http\Controllers;

use App\Models\Idea;
use App\Models\Initiative;
use App\Models\Reaction;
use Illuminate\Http\Request;

class ReactionController extends Controller
{

    public function likeIdeas(Request $request, $int)
    {
    	$idea = Idea::where('id', $int)->first();

        if ($request->user()) {
            if (Reaction::where(['user_id' => $request->user()->id, 'reactables_type' => 'App\Models\Idea', 'reactables_id' => $int])->exists()) {
                Reaction::where(['user_id' => $request->user()->id, 'reactables_type' => 'App\Models\Idea', 'reactables_id' => $int])->delete();

                $idea->like_count = $idea->like_count - 1;

            } else {
                $reaction = new Reaction;
                $reaction->user_id = $request->user()->id;
                $reaction->reactable()->associate($idea);
                $reaction->save();

                $idea->like_count = $idea->like_count + 1;
            }
        } else {
            if ($request->action == 'add') {
                $idea->like_count = $idea->like_count + 1;
            }else {
                $idea->like_count = $idea->like_count - 1;
            }
        }

        $idea->save();

    	return response()->json([
	      'status' => 'OK',
	      'type' => 'like',
	      'data' => $idea->like_count
	    ]);
    }

    public function likeInitiative(Request $request, $int)
    {
    	$initiative = Initiative::where('id', $int)->first();

        if ($request->user()) {
        	if (Reaction::where(['user_id' => $request->user()->id, 'reactables_type' => 'App\Models\Initiative', 'reactables_id' => $int])->exists()) {
        		Reaction::where(['user_id' => $request->user()->id, 'reactables_type' => 'App\Models\Initiative', 'reactables_id' => $int])->delete();
        		
                $initiative->like_count = $initiative->like_count - 1;

        	} else {
    	    	$reaction = new Reaction;
    	    	$reaction->user_id = $request->user()->id;
    	    	$reaction->reactable()->associate(Initiative::find($int));
    	        $reaction->save();
        		
                $initiative->like_count = $initiative->like_count + 1;
        	}
        } else {
            if ($request->action == 'add') {
                $initiative->like_count = $initiative->like_count + 1;
            }else {
                $initiative->like_count = $initiative->like_count - 1;
            }
        }

        $initiative->save();


    	return response()->json([
	      'status' => 'OK',
	      'type' => 'like',
	      'data' => $initiative->like_count
	    ]);
    }
}

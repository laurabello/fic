<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {

        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Category::class);

    	$category = new Category;

        return view('admin.categories.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Category::class);

        $category = Category::create($this->validateRequest($request));

        $this->storeImage($category);

        $category->save();

        return redirect()->route('categories.index')->with('message', 'Catégorie créée');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
		return view('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('create', Category::class);
        
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {

        $this->authorize('update', $category);

        $category->update($this->validateRequest($request));

        $this->storeImage($category);

        $category->save();

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->authorize('delete', $category);

        $category->delete();

      return redirect()->route('categories.index');
    }

    private function validateRequest($request)
    {
        $request['slug'] = Str::slug($request['name']);
        $request['picture'] = '';

        return request()->validate([
            'name' => 'required|min:3',
            'slug' => 'required|min:3',
            'description' => 'nullable',
            'picture' => 'sometimes|file|image|max:5000',
        ]);
 
    }

    private function storeImage($category) {
        if(request()->has('picture')){
            $category->update([
                'picture'=> request()->picture->store('uploads/category', 'public'),
            ]);
        }
    }
}

    
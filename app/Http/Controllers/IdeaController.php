<?php

namespace App\Http\Controllers;

use App\Models\Idea;
use App\Models\Category;
use Illuminate\Http\Request;

class IdeaController extends Controller
{
    public function index()
    {
        $ideas = Idea::orderBy('created_at', 'desc')->get();
        $categories = Category::all();

        return view('ideas.index', compact('ideas', 'categories'));
    }

    public function store(Request $request)
    {
        $ajax = $request->expectsJson();

        $idea = Idea::create($this->validateIdea($request));
        $idea->save();
        
        if ($ajax) {
            return response()->json([
              'status' => 'OK',
              'type' => 'idea',
              'data' => $idea->id
            ]);
        }

        return redirect()->route('ideas.index')->with('message', 'Idée créée');
    }

    public function show(Request $request, Idea $idea)
    {
    	$ajax = $request->expectsJson();

    	$idea = $idea->load(['author', 'category']);

    	if ($ajax) {
          return response()->json([
              'status' => 'OK',
              'type' => 'idea',
              'data' => $idea
              ]
          );
        }

		return view('ideas.show', compact('idea'));
    }

    /**
     * Function to retrieve the two most recent ideas
     */
    public function homeIdeaRecent(Request $request)
    {
      $ajax = $request->expectsJson();
      $ideas = Idea::orderBy('created_at')->limit(2)->get();
    	if ($ajax) {
        return response()->json([
          'status' => 'OK',
          'type' => 'idea',
          'data' => $ideas
          ]
        );
      }
    }

    /**
     * Function to retrieve the most popular idea
     */
    public function homeIdeaPopular(Request $request)
    {
      $ajax = $request->expectsJson();
      $idea = Idea::orderBy('like_count', 'desc')->limit(1)->get();
      
    	if ($ajax) {
        return response()->json([
          'status' => 'OK',
          'type' => 'idea',
          'data' => $idea
          ]
        );
      }
    }

    public function ideaByCat(Category $category)
    {
      $ideas = Idea::with('category')->where('category_id', '=', $category->id)->get();
      return view('ideas.ideaByCat', compact('ideas', 'category'));
    }

    public function transformIdea(Idea $idea) 
    {
      $req = 'initiative';
      return view('suggest.index', compact('idea', 'req'));
    }

    public function destroy(Request $request, Idea $idea) {
        $idea->delete();

        if($request->expectsJson()) {
          return response()->json([
            'status' => 'OK',
            'type' => 'idea',
          ]
          );
        }
    }

    private function validateIdea($request)
    {
        return request()->validate([
          'name' => 'required|min:3',
          'description' => 'nullable|string',
          'category_id' => 'nullable|integer',
          'owner_status' => 'nullable|integer',
          'owner_firstname' => 'nullable|string|min:3',
          'owner_lastname' => 'nullable|string|min:3',
          'owner_mail' => 'nullable|string|min:3',
          'owner_phone' => 'nullable|string|min:3'
        ]);
    }
}

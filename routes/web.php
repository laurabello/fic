<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\FicConfigController;
use App\Http\Controllers\Admin\StaticPageController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\InitiativeController;
use App\Http\Controllers\IdeaController;
use App\Http\Controllers\SuggestController;
use App\Http\Controllers\CollectiveController;
use App\Http\Controllers\ReactionController;
use App\Http\Controllers\ModerateController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PhotoController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();


Route::get('/', 'HomeController@index')->name('home');


// Admin Routes
Route::namespace('Admin')->prefix('/admin')->group(function () {

	Route::get('/', [AdminController::class, 'index'])->name('adminHome');

	Route::get('/config', [FicConfigController::class, 'edit'])->name('configuration');
	Route::put('/config/header', [FicConfigController::class, 'updateHeader'])->name('configuration.updateHeader');
	Route::put('/config/footer', [FicConfigController::class, 'updateFooter'])->name('configuration.updateFooter');
	Route::put('/config/socialize', [FicConfigController::class, 'updateSocialize'])->name('configuration.updateSocialize');
	Route::post('/config/socialize', [FicConfigController::class, 'addSocialize'])->name('configuration.addSocialize');
	Route::delete('/config/socialize-delete/{ficConfig}', [FicConfigController::class, 'deleteSocialize'])->name('configuration.deleteSocialize');
  Route::get('/collectives', [CollectiveController::class, 'showAdmin'])->name('collectives.admin');
  Route::get('/collectives/ref-collectives', [CollectiveController::class, 'refColl'])->name('collectives.ref-coll');
  Route::get('/collectives/member-collectives', [CollectiveController::class, 'memberColl'])->name('collectives.member-coll');
  Route::get('/collectives/create', [CollectiveController::class, 'createColl'])->name('collectives.create');
  Route::get('/profil', [UserController::class, 'dashboard'])->name('dashboard');
  Route::get('/messages', [ContactController::class, 'index'])->name('all_messages');
  Route::get('/message/{contact}', [ContactController::class, 'showMessage'])->name('show_message');
  Route::get('/initiatives', [InitiativeController::class, 'indexAdmin'])->name('initiatives-admin');
});

Route::resources([
	'/admin/profile' => Admin\UserController::class,
	'/admin/categories' => 'CategoryController',
  '/admin/static-pages' => Admin\StaticPageController::class
]);

Route::post('/user/add-picture', [UserController::class, 'addPicture']);
Route::delete('/user/delete-picture/{user}', [UserController::class, 'deletePicture']);
Route::post('/user/ban', [UserController::class, 'ban']);

// Initiatives Routes
Route::resource('/initiatives', 'InitiativeController');
Route::post('/initiative', [InitiativeController::class, 'show']);
Route::post('/initiative-home', [InitiativeController::class, 'homeInitiative']);
Route::get('/initiative/{category}', [InitiativeController::class, 'initByCat']);
Route::post('/initiative-join', [InitiativeController::class, 'joinInitiative']);
Route::put('/update-initiative', [InitiativeController::class, 'updateInit']);
Route::delete('/delete-initiative/{initiative}', [InitiativeController::class, 'deleteInit']);

// Ideas Routes
Route::resource('/ideas', 'IdeaController');
Route::post('/idea-recent-home', [IdeaController::class, 'homeIdeaRecent']);
Route::post('/idea-popular-home', [IdeaController::class, 'homeIdeaPopular']);
Route::get('/idea/{category}', [IdeaController::class, 'ideaByCat']);

// Suggest Routes
Route::get('/suggest/idea', [SuggestController::class, 'index']);
Route::get('/suggest/initiative', [SuggestController::class, 'index']);
Route::get('/transform/{idea}', [IdeaController::class, 'transformIdea']);

// Save Items Routes
Route::post('/save-initiative', [InitiativeController::class, 'store'] );
Route::post('/save-idea', [IdeaController::class, 'store'] );
Route::post('/save-collective', [CollectiveController::class, 'store'] );
Route::put('/update-collective/{collective}', [CollectiveController::class, 'update']);

// Collectives Routes
Route::resource('/collectif', 'CollectiveController');
Route::post('/home-collectives', [CollectiveController::class, 'homeColl']);
Route::get('/all-collectif', [CollectiveController::class, 'indexForAdmin'])->name('all_collectives');
Route::post('/collective/add-picture', [CollectiveController::class, 'addPicture']);
Route::delete('/delete-collective/{collective}', [CollectiveController::class, 'destroy']);
Route::delete('/delete-collective-picture/{collective}', [CollectiveController::class, 'deleteImg']);

// Pages Route
Route::get('/page/{staticPage}', [StaticPageController::class, 'showForAll']);
Route::get('/contact', [ContactController::class, 'show'])->name('page_contact');
Route::post('contact/envoyer', [ContactController::class, 'store'])->name('store_contact');

Route::post('/admin/profile-name/{user}', [UserController::class, 'showName']);
Route::post('/filtered-initiatives', [InitiativeController::class, 'filterInit']);

Route::post('/like/idea/{int}', [ReactionController::class, 'likeIdeas']);
Route::post('/like/initiative/{int}', [ReactionController::class, 'likeInitiative']);

// Pages Moderate
Route::get('/admin/moderate', [ModerateController::class, 'index'])->name('moderate.index');
Route::put('/admin/moderate/reject/{moderate}', [ModerateController::class, 'reject'])->name('moderate.reject');
Route::put('/admin/moderate/validate/{moderate}', [ModerateController::class, 'valide'])->name('moderate.validate');
Route::post('/moderate', [ModerateController::class, 'signal']);


// Photos route
Route::post('/save-init-photo', [PhotoController::class, 'storeForInit'])->name('save-init-photo');

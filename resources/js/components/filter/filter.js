  export default {
    methods: {
      filter(filteredInit, currentComp, cat, free, month, selectedCat) {
        // Filter by categories chosen
        if(cat) {
          filteredInit = filteredInit.filter(e => ((selectedCat).includes(e.category_id)))
        }

        //Filter by price
        if(free) {
          filteredInit = filteredInit.filter(e => e.price == null)
        }

        // //Filter by month
        if(month) {
          //Current date
          var date = new Date();
          var nextDate = date.getDate() + 10;
          date.setDate(nextDate);

          filteredInit = filteredInit.filter(e => (new Date(e.event_start.replace(/-/g,"/"))) >= date)
        }

        return filteredInit      
      }
    }
  }
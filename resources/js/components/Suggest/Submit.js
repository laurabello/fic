export default {
  methods: {
    submitForm(e) {
      e.preventDefault()
      let currentComp = this
      let data
      let url

      if (this.request == 'idea') {
        url = '/save-idea'
        data = this.formIdea
      } else if (this.type == 'initiative') {
        url = '/update-initiative'
        data = this.formInit
        data.id = this.model.id
        data.collective_id = this.selectedColl.id
      } else {
        url = '/save-initiative'
        data = this.formInit
        data.collective_id = this.selectedColl.id
      }

      if (url == '/update-initiative') {
        console.log('pout')
        axios.put(url, data)
          .then(response => {
            currentComp.message = response.data.data
            window.location.href = '/admin/initiatives'
          })
          .catch(error => {
          currentComp.message=error
        })
      } else {
        axios.post(url, data)
          .then(function (response) {
            if (url == '/save-idea') {
              window.location.href = '/ideas'
            } else {
                axios.delete('/ideas/' + currentComp.idea.id)
                  .then(res => {
                    window.location.href = '/initiatives/' + response.data.data
                  })
                  .catch(er => {
                    console.log(er)
                  })
              }
              window.location.href = '/initiatives/' + response.data.data
            })
          .catch(function (error) {
            currentComp = error;
          })
      }

    }
  }
}
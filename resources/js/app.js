/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

if (document.getElementById('home')) {
  Vue.component('home', require('./components/Home.vue').default);
} else if (document.getElementById('ideas')) {
  Vue.component('allideas', require('./components/Ideas/AllIdeas.vue').default);
} else if (document.getElementById('initiatives')) {
  Vue.component('allinitiatives', require('./components/Initiatives/AllInitiatives.vue').default);
} else if (document.getElementById('initiative-detail')) {
Vue.component('initiative-detail', require('./components/Initiatives/InitiativeDetail.vue').default);
} else if (document.getElementById('suggest')) {
  Vue.component('suggest', require('./components/Suggest/Suggest.vue').default);
} else if (document.getElementById('initiativesByCat')) {
  Vue.component('catinitiatives', require('./components/Initiatives/CatInitiatives.vue').default);
} else if (document.getElementById('ideasByCat')) {
  Vue.component('catideas', require('./components/Ideas/CatIdeas.vue').default);
} else if (document.getElementById('dashboard')) {
  Vue.component('dashboard', require('./components/Dashboard.vue').default);
} else if (document.getElementById('allCollectives') || document.getElementById('collAdmin')) {
  Vue.component('allcollectives', require('./components/Collectives/AllCollectives.vue').default);
} else if (document.getElementById('createColl')) {
  Vue.component('addcollective', require('./components/Collectives/AddCollective.vue').default);
} else if (document.getElementById('similar_init') || document.getElementById('adminInitiatives')) {
  Vue.component('initiative', require('./components/Initiatives/Initiative.vue').default);
} else if (document.getElementById('collective_members')) {
  Vue.component('members', require('./components/Join/Members.vue').default);
} else if (document.getElementById('collList')) {
  Vue.component('checkbox', require('./components/Collectives/CheckBox.vue').default);
}

Vue.component('collective', require('./components/Collectives/Collective.vue').default);
// Vue.component('advanced-searchbox', require('./components/AdvancedSearch.vue').default);
// Vue.component('searchbox', require('./components/BasicSearch.vue').default);
// Vue.component('headernav', require('./components/MenuContainer.vue').default);
// Vue.component('containertablet', require('./components/header/ContainerTablet.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
    	cat: ''
    }
});


require('./scripts/script');



/**
 * Detect TOUCH
 */
function cantTouchThis() { return ('ontouchstart' in window) }

/* Nav */
(() => {
    let timer = null
    const h = document.getElementById('site_header')

    const startTimer = callback => {
        if(timer) {
            window.clearInterval(timer)
        }
        timer = window.setTimeout(callback, 300)
    }

    const close = (elem, force) => {
        let active = (elem.classList.contains('header_sub_menu') ? h.querySelector('.site_nav') : h).querySelector('.active')
        
        if(active) {
            if(force) {
                active.classList.remove('active')
            } else {
                startTimer(()=>{
                    active.classList.remove('active')
                })
            }
        }
    }

    const open = elem => {
        startTimer(()=>{
            close(elem, true)
            elem.classList.add('active')
        })
    }

    const init = (btn, target) => {
        btn.addEventListener('click', e => {
            if (!target.classList.contains('active')) {
                open(target)
            } else {
                close(target)
            }

            if (cantTouchThis()) {
                e.preventDefault()
            }
        })
        btn.addEventListener('keyup', e => {
            if (e.keyCode == 13) {
                if (!target.classList.contains('active')) {
                    open(target)
                } else {
                    close(target)
                }
            }
        })
        if(!btn.dataset.hover) {
            btn.parentNode.addEventListener('mouseenter', () => open(target))
            btn.parentNode.addEventListener('mouseleave', () => close(target))
        }
    }
    h.querySelectorAll('[aria-controls]').forEach(btn => {
        init(btn, h.querySelector('#'+btn.getAttribute('aria-controls')))
    })
    h.querySelectorAll('.nav_list>a').forEach(btn => {
        if (btn.parentNode.querySelector('.header_sub_menu')) {
            init(btn, btn.parentNode.querySelector('.header_sub_menu'))
        }
    })

    if (document.getElementById('ficConfigsEditor')) {
        delSocializeButton = document.querySelectorAll('.del-socialize-button')
        delSocializeButton.forEach((delSocialize) => {
            delSocialize.addEventListener('click', () => delSocializeFunc(delSocialize.id))
        })
    }
})()




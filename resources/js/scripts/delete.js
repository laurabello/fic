export default {
  methods: {
    deleteImg(url, data) {
      let deleteConfirm = confirm('Supprimer l\'image. Êtes-vous sûr?')

      if (deleteConfirm) {
        axios.delete(url)
          .then(response => {
            this.selectedFile = null
            data.picture = null
          })
          .catch(error => {
            this.message = error
          })
      }
    },
    deleteInstance(url) {
      let deleteConfirm = confirm('Supprimer. Êtes-vous sûr?')

      if (deleteConfirm) {
        axios.delete(url)
          .then(response => {
            this.currentComp = null
            this.selectedFile = null
          })
          .catch(error => {
            this.message = error
          })
      }
    }
  }
}
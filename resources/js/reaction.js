import axios from 'axios';

export default
{
  methods: 
  {
    like (e) {
      let elem = ""
      console.log(e)

      if (e.target.nodeName == "BUTTON") {
        elem = e.srcElement
      } else {
        elem = e.target.parentNode
      }

      let url = elem.attributes.href.value
      let type = url.split('/')[2]
      let id = url.split('/')[3]

      if (localStorage.getItem('like'+type)) {
        let value = JSON.parse(localStorage.getItem('like'+type))
        let isIn = ""

        value.forEach((e, i) => {
          if(id && e==id) {
            value.splice(i, 1)
            addLike(url, elem, 'del')
            isIn = true
          }
        })

        if(id && !isIn) {
          value.push(id)
          addLike(url, elem, 'add')
        }

        localStorage.setItem('like'+type, '['+value+']')
      } else {
        addLike(url, elem, 'add')
        localStorage.setItem('like'+type, '['+id+']')
      }

      function addLike(url, event, action) {
        axios.post(url, {action: action})
        .then(function (response) {
          event.lastChild.innerText = response.data.data
        })
        .catch(function (error) {
          //currentComp = error;
        })
      }

    },
    moderate (e) {
      let elem = ""

      if (e.originalTarget.nodeName == "A") {
        elem = e.explicitOriginalTarget
      } else {
        elem = e.originalTarget.parentNode
      }

      let type = elem.dataset.type
      let id = elem.dataset.id

      let data = {
        id: id,
        type: type
      }
      if (localStorage.getItem('moderate'+type)) {
        let value = JSON.parse(localStorage.getItem('moderate'+type))
        let isIn = ""

        value.forEach((e, i) => {
          if(id && e==id) {
            isIn = true
            console.log(e, i, isIn)
          }
        })

        if(id && !isIn) {
          value.push(id)
          moderate('/moderate', elem, data)
        }

        localStorage.setItem('moderate'+type, '['+value+']')
      } else {
        moderate('/moderate', elem, data)
        localStorage.setItem('moderate'+type, '['+id+']')
      }


      function moderate(url, event, action) {
        axios.post(url, action)
        .then(function (response) {
          event.lastChild.innerText = response.data.data
        })
        .catch(function (error) {
          //currentComp = error;
        })
      }

    }
  }
}

import axios from 'axios';

export default
{
  methods: 
  {
    /**
     * Function to retrieve datas from the database
     * @param {string} method One of the CRUD method (get, post, put, delete)
     * @param {string} url The route url for the request
     * @param {string} params The parameters for the request
     * These parameters are retrieved from the datas of the components wich is calling the method
     */
    async getData(method, url, params) {
      let response
      try {
        switch (method) {
          case 'get':
            response = await axios.get(url, params);
            break;
          case 'post':
            response = await axios.post(url, params);
            break;
          case 'put':
            response = await axios.put(url, params);
            break;
          case 'delete':
            response = await axios.delete(url, params);
            break;
        }
        this.datas = response.data.data;
      } catch (error) {
        console.log(error);
      }
    }
  },
  mounted() {
    this.getData(this.method, this.url, this.params)
  }
}

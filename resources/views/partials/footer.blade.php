<footer class="site_footer">
    <div class="logo_footer">
    <a class="footer_logo" href="{{ url('/') }}">
        @if ($ficConfigs['picture']['value'])
            <img src="{{ asset('storage/'.$ficConfigs['picture']['value']) }}" alt="{{ __('Retourner sur la page d\'accueil de') }} {{ $ficConfigs['name']['value'] }}" width="100">
        @else
            {{ $ficConfigs['name']['value'] }}
        @endif
        </a>        <address class="address_footer">
            {{ $ficConfigs['name']['value'] }}<br>
            @if ($ficConfigs['address1']['value'])
            {{ $ficConfigs['address1']['value'] }}<br>
            @endif
            @if ($ficConfigs['address2']['value'])
            {{ $ficConfigs['address2']['value'] }}<br>
            @endif
            @if ($ficConfigs['city']['value'])
            {{ $ficConfigs['zipcode']['value'] }} {{ $ficConfigs['city']['value'] }}<br>
            @endif
        </address>
    </div>
    <nav class="links_footer">
        <a href="/page/la-charte">Notre charte</a>
        <a href="/page/mentions-legales">Mentions légales</a>
        <a href="{{route('page_contact')}}">Contact</a>
    </nav>
    <div class="social_footer">
        @foreach ($ficConfigs['socialize'] as $socialize)
        <a href="{{ $socialize['url'] }}"><i class="{{'fab fa-'. $socialize['logo']}}"></i>{{ $socialize['nom'] }}</a>
        @endforeach
    </div>
</footer>

<header class="site_header" id="site_header">
    <div class="header_top">
        <a class="header_logo" href="{{ url('/') }}">
        @if ($ficConfigs['picture']['value'])
            <img src="{{ asset('storage/'.$ficConfigs['picture']['value']) }}" alt="{{ __('Retourner sur la page d\'accueil de') }} {{ $ficConfigs['name']['value'] }}" width="100">
        @else
            {{ $ficConfigs['name']['value'] }}
        @endif
        </a>

        @guest
        <a class="header_login" href="{{ route('login') }}">
            <i class="fas fa-user"></i>
            {{ __('Se connecter') }}
        </a>
        @endguest
        @auth
        <div class="header_login">
            <button aria-controls="user_menu">
                <i class="fas fa-user"></i>
                <span>{{ Auth::user()->name }}</span>
            </button>
            <ul class="user_menu" id="user_menu" aria-expanded="false">
                <li>
                    <a href="{{route('profile.index')}}">profile</a>
                </li>
                @can('create', App\Models\Category::class)
                <li>
                    <a href="{{route('adminHome')}}">admin</a>
                </li>
                @endcan
                <li>
                    <a class="logout" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
        @endauth
    </div>

</header>
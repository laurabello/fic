<header class="site_header" id="site_header">
    <div class="header_top">
        <a class="header_logo" href="{{ url('/') }}">
        @if ($ficConfigs['picture']['value'])
            <img src="{{ asset('storage/'.$ficConfigs['picture']['value']) }}" alt="{{ __('Retourner sur la page d\'accueil de') }} {{ $ficConfigs['name']['value'] }}" width="100">
        @else
            {{ $ficConfigs['name']['value'] }}
        @endif
        </a>

        <!-- toggle links -->
        <button class="header_toggle" aria-label="{{ __('Voir le menu')}}" aria-controls="header_nav" data-hover="none"><i class="fas fa-bars"></i></button>
        <button class="header_toggle" aria-label="{{ __('Afficher la recherche')}}" aria-controls="header_search" data-hover="none"><i class="fas fa-search"></i></button>

        <!-- Search -->
        <div class="header_search" id="header_search">
            <form>
                <input type="search" name="src" aria-label="{{ __('Rechercher') }}">
                <select name="type" id="type" aria-label="{{ __('Rechercher dans') }}">
                    <option value="initiatives">{{ __('Initiatives') }}</option>
                    <option value="idees">{{ __('Idées') }}</option>
                </select>
                <button class="btn_search" type="submit"><i class="fas fa-search" aria-label="{{ __('rechercher') }}"></i></button>
                <button class="more_search">{{ __('Recherche avancée') }}</button>
            </form>
        </div>
        <!-- Authentication Links -->
        @guest
        <a class="header_login" href="{{ route('login') }}">
            <i class="fas fa-user"></i>
            {{ __('Se connecter') }}
        </a>
        @endguest
        @auth
        <div class="header_login">
            <button aria-controls="user_menu">
                <i class="fas fa-user"></i>
                <span>{{ Auth::user()->name }}</span>
            </button>
            <ul class="user_menu" id="user_menu" aria-expanded="false">
                <li>
                    <a href="{{route('dashboard')}}">profil</a>
                </li>
                @can('create', App\Models\Category::class)
                <li>
                    <a href="{{route('adminHome')}}">admin</a>
                </li>
                @endcan
                <li>
                    <a class="logout" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
        @endauth
    </div>
    <div class="header_bottom">
        <nav class="site_nav" id="header_nav">
            <ul>
                <li class="nav_list">
                    <a href="{{ route('initiatives.index') }}">
                        {{ __('Initiatives') }}
                        <i class="fas fa-chevron-down"></i>
                    </a>
                    <ul class="header_sub_menu">
                        @foreach ($categories as $categorie)
                        <li>
                            <a href="/initiative/{{ $categorie->slug }}">
                                <span><img  class="" src="{{asset('storage/'. $categorie->picture)}}" alt="{{$categorie->name}}" ></span>
                                {{ $categorie->name }}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                <li class="nav_list">
                    <a href="/ideas">
                        {{ __('Idées') }}
                        <i class="fas fa-chevron-down"></i>
                    </a>
                    <ul class="header_sub_menu">
                        @foreach ($categories as $categorie)
                        <li>
                            <a href="/idea/{{ $categorie->slug }}">
                            <span><img  class="" src="{{asset('storage/'. $categorie->picture)}}" alt="{{$categorie->name}}" ></span>
                                {{ $categorie->name }}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                <li class="nav_list">
                    <a href="/page/la-fic">
                        {{ __('Qui sommes nous ?') }}
                    </a>
                </li>
                <li class="nav_list">
                    <a href="{{ route('collectif.index') }}">
                        {{ __('Collectifs') }}
                    </a>
                </li>
                <li class="nav_list propose">
                    <a class="link_propose" href="#">
                        {{ __('Proposer') }}
                        <i class="fas fa-chevron-down"></i>
                    </a>
                    <ul class="header_sub_menu">
                        <li>
                            <a href="/suggest/idea">
                            {{ __('Une idée') }}
                            </a>
                        </li>
                        <li>
                            <a href="/suggest/initiative">
                                
                                {{ __('Une initiative') }}
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

    </div>
</header>
<!--
<form class="advanced_search" action="">
    <div class="advanced_half">
        <div class="group_field">
            <label for="location">Où?</label>
            <input type="text" name="location">

        </div>

        <div class="group_field">
            <label for="date">Quand?</label>
            <input type="date" name="initDate">

        </div>
    </div>
    <div class="advanced_half">
        <div class="group_field">
            <label for="category">Catégorie</label>
            <select v-model="cat" name="type" id="type">
            @foreach ($categories as $categorie)
                <option value="{{$categorie->id}}">{{ $categorie->name }}</option>
                @endforeach

            </select>
        </div>

        <div class="group_field">
            <label for="keyword">Mot clé</label>
            <input type="text" name="keyword">

        </div>
    </div>

    <input class="btn" type="submit" value="rechercher">
</form>
-->

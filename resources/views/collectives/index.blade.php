@extends('layouts.app')

@section('content')
<h1 class="section_title">Tous les collectifs</h1>
<div id="allCollectives">
  <AllCollectives :collectives="{{json_encode($collectives)}}"/>
</div>
@endsection

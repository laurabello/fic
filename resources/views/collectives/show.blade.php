@extends('layouts.app')

@section('content')
@php
if (Auth::user()) {
  $user = Auth::user();
} else {
  $user = Null;
}
@endphp
<div class="content_container">
  <h1 class="section_title">Collectif {{$collectif->name}} </h1>
  <div class="coll_presentation">
    @if ($collectif->picture)
    <img class="coll_pic" src="/storage/{{$collectif->picture}}" alt="{{ $collectif->name }}" class="collAvatar">
    @endif
    <div class="coll_infos">
      <div class="coll_details">
        <address class="coll_address"> 
        <i class="fas fa-map-marked-alt"><p>Adresse:</p></i><br>        
          {{$collectif->address1}}<br>
          {{$collectif->address2}}<br>
          {{$collectif->zip_code}} {{$collectif->city}}
        </address>
        <!-- <div class="coll_contact">
          <i class="fas fa-at"><p>mail</p></i>
          <i class="fas fa-phone-alt"><p>telephone</p></i>
        </div> -->
        <div class="coll_links">
          <i class="fas fa-globe"><p><a href="{{$collectif->url}}">{{$collectif->url}}</a></p></i>
          <!-- <i class="fab fa-facebook"><p>social networks</p></i> -->
        </div>
      </div>
      <div class="coll_description">
        <h2 class="title">Description:</h2>
        <p>{{$collectif->description}}</p>
      </div>
    </div>
  </div>

  <div class="init_detail_orga">
    <div class="member">
      <h3 class="small_title">Organisateur</h3>
      @if ($collectif->owner->picture)
      <div class="member_pic">
        <img src="/storage/{{ $collectif->owner->picture }}" :alt="{{ $collectif->owner->name }}">
      </div>
      @else
      <div class="member_no_pic">{{ $collectif->owner->firstname[0] }}
      </div>
      @endif
      <div>{{ $collectif->owner->name }} {{ $collectif->owner->firstname[0] }}
      </div>
    </div>
    <div id="collective_members">
      <Members :id="{{ $collectif->id }}" :type="'collective'" :members="{{ json_encode($collectif->members) }}" :user="{{ json_encode($user) }}" :orga="{{ json_encode($collectif->owner) }}" />
    </div>
  </div>
  @if ($collectif->values)
  <div class="collValues">
    <h2 class="section_title">Nos valeurs</h2>
    <p>{{$collectif->values}}</p>
  </div>
  @endif
  @if ($collectif->initiative)
  <div class="collInitiatives">
    <h2 class="section_title">Nos initiatives</h2>
      <AllInitiatives :datas="{{json_encode($collectif->initiative)}}"/>
  </div>
  @endif
</div>
@endsection

@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
  <h1 class="section_title">Bienvenue {{ Auth::user()->name }}</h1>
  @can('viewAny', App\User::class)
    <h2>Administration du site</h2>
    <div>
      <p>Ici vous pouvez ajouter ou modifier une catégorie, modifier les pages statiques et gérer la configuration du site.</p>
    </div>
  @endcan
    <div>
      Vous pouvez gérer votre profil, vos collectifs et initiatives.
    </div>
</div>

@endsection


@extends('layouts.admin')

@php

$header = ['name', 'picture'];
$footer = ['address1', 'address2', 'zipcode', 'city', 'phone', 'mail', 'longitude', 'latitude', 'googleMaps'];

@endphp

@section('content')
@include('admin.menu')
<div id="ficConfigsEditor" class="content_container">
    <h1 class="section_title">Configuration</h1>

  <div class="config_form">

      <h2 class="title">Identité du site</h2>
      <form action="{{ route('configuration.updateHeader') }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="inputs_footer">
        @foreach ($ficConfigs as $config)
        @if (in_array($config->option, $header))
        @if ($config->option == 'name')
        <div class="group_logo">
          <label>{{ $config->option_sub }}</label>
          <input name="{{ $config->option }}" type="text" value="{{ $config->value }}">
        </div>
        @endif
        @if ($config->option == 'picture')
        <div class="group_logo">
          <label for="picture">{{ $config->option_sub }}</label>
          <input id="picture" name="picture" type="file" value="{{ old('picture') ?? $config->picture }}">
        </div>
        @endif
        @endif
        @endforeach
        </div>
        <div class="form_button">
          <input class="btn" type="submit" value="Valider">
        </div>

      </form>
</div>
<div class="config_form">
      <h2 class="title">Footer</h2>
      <form action="{{ route('configuration.updateFooter') }}" method="post">
        @method('PUT')
        @csrf
        <div class="inputs_footer">
        @foreach ($ficConfigs as $config)
        @if (in_array($config->option, $footer))

        <div class="group_field">
          <label>{{ $config->option_sub }}</label>
          <input name="{{ $config->option }}" type="text" value="{{ $config->value }}">
        </div>

        @endif
        @endforeach
        <div class="form_button">
        <input class="btn" type="submit" value="Valider">
        </div>
</div>
      </form>
</div>
<div class="social_config">
<h2 class="title">Réseaux sociaux</h2>
<div class="social border_bottom">

    <form action="{{ route('configuration.updateSocialize') }}" method="post">
      @method('PUT')
      @csrf
      <div class="social_wrap">
      @foreach ($ficConfigs as $config)
        @if ($config->option == 'socialize')
        <div class="group_field">
          <label>{{ $config->option_sub }}</label>
          <input name="{{ $config->option_sub }}" type="text" value="{{ $config->value }}">
          <i id="del-socialize-{{ $config->id }}" class="fas fa-trash del-socialize-button"><span>supprimer</span></i>
        </div>
        @endif
      @endforeach
    </div>
    <div class="form_submit">
      <input class="btn" type="submit" value="Valider">
</div>
    </form>
    </div>
<div class="social">
    <h3>Ajouter un réseau social</h3>
    <form action="{{ route('configuration.addSocialize') }}" method="post">
      @method('POST')
      @csrf
      <div class="group_field">
        <label for="name">Nom</label>
        <input id="name" name="name" type="text" placeholder="Nom du réseau">
      </div>
      <div class="group_field">
        <label for="url">Url</label>
        <input id="url" name="url" type="text" placeholder="Adresse du réseau">
      </div>
      <div class="group_field">
        <label>Picto</label>
        
      </div>
      <div class="form_submit">
      <input class="btn" type="submit" value="Valider">
</div>
    </form>

  </div>
</div>
</div>



@endsection

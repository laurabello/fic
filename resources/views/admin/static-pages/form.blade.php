@csrf
<div class="group_field">
	<label for="title">{{ __('Titre') }}:</label>
	<input id="title" name="title" type="text" value="{{ old('title') ?? $staticPage->title }}">
	<div class="form-errors">{{ $errors->first('title') }}</div>
</div>

<div class="group_field">
	<label for="content">{{ __('Contenu') }}:</label>

	<textarea id="content" name="content" type="text" col="15" rows="5" value="">{{ old('content') ?? $staticPage->content }}</textarea>

	<div class="form-errors">{{ $errors->first('content') }}</div>
</div>

{{-- <div class="group_field">
	<label for="photo">{{ __('Picture') }}:</label>
	<input id="photo" name="photo" type="file" value="{{ old('picture') ?? $staticPage->picture }}">
</div> --}}

<div class="btn_send">
	<input class="btn" name="submit" type="submit" value="{{ __('Modifier') }}">
</div>

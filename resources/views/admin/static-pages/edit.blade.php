@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">

  <div class="edit_page">
    <h1 class="text_center">Page : {{ $staticPage->title }}</h1>

    <form class="form_edit" action="{{ route('static-pages.update', $staticPage) }}" method="post">
      @method('PUT')
      @include('admin.static-pages.form')
    </form>
  </div>
  <div class="edit_return">
    <a class="btn" href="{{route('static-pages.index')}}">Retour</a>
  </div>
</div>
@endsection

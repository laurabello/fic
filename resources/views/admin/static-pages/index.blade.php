@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
  <div>
    <h1 class="section_title">Modifier une page statique</h1>
  </div>
    <div class="">
      @foreach ($staticPages as $staticPage)
        <div class="page_container">
        <h2 class="pageTitle">Titre: {{ $staticPage->title }}</h2>
        <p>Description:</p>
        <p>{{ $staticPage->description }}</p>
        <div class="form_submit">
          <a class="btn" href="{{ route('static-pages.edit', $staticPage) }}">Modifier la page</a>
        </div>
      </div>
    @endforeach
  </div>
</div>

@endsection

@extends('layouts.admin')

@section('content')
@include('admin.menu')

<div class="content_container">
<h1 class="section_title">Gérer vos collectifs</h1>
  <div class="collectives_links">
    <a class="btn" href="{{route('collectives.ref-coll')}}">Voir les collectifs dont vous êtes référent</a>
    <a  class="btn" href="{{route('collectives.member-coll')}}">Voir les collectifs dont vous êtes membre</a>
    <a  class="btn" href="{{route('collectives.create')}}">Créer un nouveau collectif</a>
    @can('create', App\Models\FicConfig::class)
      <a class="btn" href="{{route('all_collectives')}}">Voir tous les collectifs</a>
    @endcan
  </div>
</div>

@endsection
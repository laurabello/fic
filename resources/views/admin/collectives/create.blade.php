@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
  <div id="createColl">
    <h1 class="section_title">Créer un nouveau collectif</h1>
    <addcollective :admin="{{true}}"/>
  </div>
  <div class="return">
  <a class="btn" href="{{route('collectives.admin')}}">Retour</a>
</div>
</div>
@endsection
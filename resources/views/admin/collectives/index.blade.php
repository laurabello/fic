@extends('layouts.app')

@section('content')
<div id="collList">
	<h1 class="section_title">Liste de tous les collectifs</h1>

	<div class="coll_list">
	@foreach ($collectives as $collective)
	<div>
	    <a href="collectif/{{$collective->id}}" title="Voir le détail du collectif"><h2>{{$collective->name}}</h2></a>
	    <p>{{$collective->description}}</p>
	    <div>Initiatives : {{$collective->initiatives_count}}</div>
	    <div>Membres : {{$collective->members_count + 1 }}</div>
	    <div>
	      <checkbox :collective="{{json_encode($collective)}}"/>
	    </div>
    </div>
	@endforeach
	</div>
</div>
@endsection

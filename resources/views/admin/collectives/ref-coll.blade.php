@extends('layouts.admin')

@section('content')
@include('admin.menu')

@php
if (Auth::user()) {
  $user = Auth::user();
} else {
  $user = Null;
}
@endphp

<div class="content_container">
  <div id="collAdmin">
    <h1 class="section_title">Les collectifs dont vous êtes référent</h1>
    @foreach ($collectives as $collective)
    <div>
    <collective :collectives="{{json_encode($collectives)}}" :collective="{{json_encode($collective)}}" :admin="{{true}}"/>
    </div>
    <div id="collective_members">
      <Members :id="{{ $collective->id }}" :type="'collective'" :members="{{ json_encode($collective->members) }}" :user="{{ json_encode($user) }}" :orga="{{ json_encode($user) }}" />
    </div>
    @endforeach
  </div>
  <div class="return">
    <a class="btn" href="{{route('collectives.admin')}}">Retour</a>
  </div>
</div>
@endsection
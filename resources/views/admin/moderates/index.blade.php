@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
    <h1 class="admin_section_title">Modération</h1>
    <p>Eléments signalés par les utilisateurs : </p>
    <div>
      <h2>Idées reportées</h2>
      <table>
        <thead>
          <tr>
            <th>Compte</th>
            <th>Idée</th>
            <th>Description</th>
            <th>Modérer</th>
            <th>Valider</th>
          </tr>
        </thead>
      @foreach ($ideas as $idea)
        <tbody>
          <tr>
            <td>{{ $idea['num_reports'] }}</td>
            <td>{{ $idea['moderables']['name'] }}</td>
            <td>{{ $idea['moderables']['description'] }}</td>
            <td>
              <form action="{{ route('moderate.reject', $idea['id']) }}" method="post">
                @method('PUT')
                @csrf
                <input type="submit" value="Modérer">
              </form>
            </td>
            <td>
              <form action="{{ route('moderate.validate', $idea['id']) }}" method="post">
                @method('PUT')
                @csrf
                <input type="submit" value="Valider">
              </form>
          </tr>
        </tbody>
      @endforeach
      </table>




</div>

@endsection

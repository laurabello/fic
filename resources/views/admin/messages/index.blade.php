@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
<h1 class="section_title">Les messages des utilisateurs</h1>
<table>
	<thead>
		<tr>
			<th>Message de</th>
			<th>envoyé le</th>
			<th>titre</th>
			<th>Détails</th>
		</tr>

	</thead>
<tbody>
@foreach($contacts as $contact)
<tr>
	<td>
    <p>Message de {{$contact->firstname}} : </p>
    </td>
	<td>
    <p>Envoyé le {{date('j-m-Y', strtotime($contact->created_at))}} à {{date('H', strtotime($contact->created_at))}}h</p>
</td>
<td>
<p>{{$contact->title}}</p>
</td>
<td>
<a href="{{route('show_message', $contact)}}">Lire</a>
</td>
</tr>
</div>
@endforeach
@endsection

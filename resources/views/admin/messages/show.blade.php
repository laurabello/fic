@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
  <h1 class="section_title">
    Message de {{$contact->firstname}}
    @if($contact->lastname)
    {{$contact->lastname}}
    @endif
  </h1>
  <p>Envoyé le {{date('j-m-Y', strtotime($contact->created_at))}} à {{date('H', strtotime($contact->created_at))}}h</p>


    <div>
      <h2>Contact :</h2>
      <p>Email : {{$contact->email}}</p>
      @if($contact->phone)
      <p>téléphone : {{$contact->phone}}</p>
      @endif
    </div>

    <div>
      <h2>Titre</h2>
      <p>{{$contact->title}}</p>
      <h2>Message</h2>
      <p>{{$contact->message}}</p>
    </div>

</div>
@endsection

@extends('layouts.admin')

@section('content')
@include('admin.menu')

<div class="content_container">
  <h1 id="adminInitiatives" class="section_title">Gérer vos initiatives</h1>
  @foreach ($collectives as $collective)
    <h2>{{$collective->name}}</h2>
    @foreach($collective->initiatives as $initiative)
    <div>
      <initiative :initiative="{{json_encode($initiative)}}" :admin="{{true}}" :user="{{json_encode($user)}}" :categories="{{json_encode($categories)}}"/>
    </div>
    @endforeach
  @endforeach
<div>
@endsection
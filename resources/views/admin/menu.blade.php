
<div class="admin_menu">
    <a href="{{route('adminHome')}}">Home</a>
    <a href="{{route('dashboard')}}">Profil</a>
    <a href="{{route('collectives.admin')}}">Collectifs</a>
    <a href="{{route('initiatives-admin')}}">Initiatives</a>
    @can('viewAny', App\User::class)
    <a href="{{route('profile.index')}}">Tous les utilisateurs</a>
    @endcan
    @can('create', App\Models\Category::class)
    <a href="{{route('categories.index')}}">Catégories</a>
    @endcan
    @can('create', App\Models\StaticPage::class)
    <a href="{{route('static-pages.index')}}">Pages</a>
    @endcan
    @can('viewAny', App\Models\Moderate::class)
    <a href="{{route('moderate.index')}}">Modération</a>
    @endcan
    @can('create', App\Models\FicConfig::class)
    <a href="{{route('configuration')}}">Configuration</a>
    @endcan
    @can('viewAny', App\Models\Contact::class)
    <a href="{{route('all_messages')}}">Messages</a>
    @endcan
</div>

@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div id="dashboard" class="content_container">
	<dashboard :user="{{ json_encode($user) }}"/>
</div>

@endsection
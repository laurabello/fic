@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
<h1>{{ Auth::user()->name }}</h1>
<table>
	<thead>
		<tr>
			<th>name</th>
			<th>mail</th>
			<th>tel</th>
			<th>role</th>
			<th>edit</th>
		</tr>

	</thead>
<tbody>
@foreach ($users as $user)
	<tr>
		<td>
			<a href="{{ route('profile.show', $user) }}">
			{{ $user->name }} {{ $user->firstname }}
			</a> 
		</td>
		<td>
			{{ $user->email }} 
		</td>
		<td>
			{{ $user->phone }}
		</td>
		<td>
			{{ $user->role }}
		</td>
		<td>
			<a href="{{ route('profile.show', $user) }}">
			editer
			</a> 
		</td>
	</tr>
@endforeach
</tbody>
</table>
</div>

@endsection
@csrf
<div class="cat_edit">
	<label for="name">{{ __('Nom') }}:</label>
	<input id="name" name="name" type="text" value="{{ old('name') ?? $category->name }}">
	<div class="form-errors">{{ $errors->first('name') }}</div>
</div>

<div class="cat_edit">
	<label for="description">{{ __('Description') }}:</label>
	<textarea id="description" name="description" type="text" cols="30" rows="10" value="{{ old('description') ?? $category->description }}"></textarea>
	<div class="form-errors">{{ $errors->first('description') }}</div>
</div>
<div class="cat_edit">
	<label for="logo">{{ __('Picture') }}:</label>
	<input id="logo" name="logo" type="file" value="{{ old('picture') ?? $category->picture }}">
</div>

<div class="btn_send">
	@if($category->id)
	<input class="btn" name="submit" type="submit" value="{{ __('Modifier') }}">
	@else
	<input  class="btn" name="submit" type="submit" value="{{ __('Ajouter') }}">
	@endif
</div>



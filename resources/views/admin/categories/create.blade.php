@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
	<div>
<a class="btn"  href="{{route('categories.index')}}">retour</a>
</div>
<div class="form_div">	
  <h1 class="text_center">Nouvelle Catégorie</h1>
  <div>
  	<form class="form_edit" action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data">
	@method('POST')
  	@include('admin.categories.form')

  	</form>
  </div>
</div>
</div>
@endsection

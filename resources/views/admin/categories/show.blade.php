@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
  <div class="category_container">
        <h1 class="section_title">Catégorie : {{ $category->name }}</h1>
        <div class="cat_details">
          <div>
            <h2>Description de la catégorie:</h2>
            <p>{{ $category->description }}</p>
          </div>
          <div>
            <h2>Logo de la catégorie:</h2>
            @if($category->picture)
            <img  class="img_cat" src="{{asset('storage/'. $category->picture)}}" >
            @endif
          </div>
        </div>
  </div>
  <div class="return">
    <a class="btn"  href="{{route('categories.index')}}">retour</a>
    <a class="btn" href="{{route('categories.edit', $category)}}">éditer la catégorie</a>

  </div>
</div>
@endsection

@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">
    <h1 class="section_title">Catégories</h1>
    <p>Modifier les catégories existantes : </p>
    <div class="cat_display">
      @foreach ($categories as $category)
      <a class="category" href="{{ route('categories.show', $category) }}">
          
          <h2 class="cat_title">{{ $category->name }}</h2>
        <div>
            <img class="img_cat" src="{{asset('storage/'. $category->picture)}}" >
        </div>
      </a>
      @endforeach


  </div>
  <div class="add_cat">
      <p>OU</p>
      <a class="btn_large" href="{{route('categories.create')}}">Ajouter une nouvelle catégorie</a>
    </div>

</div>

@endsection

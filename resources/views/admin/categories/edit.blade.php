@extends('layouts.admin')

@section('content')
@include('admin.menu')
<div class="content_container">

<div class="form_div">
  <h1 class="section_title">Catégorie : {{ $category->name }}</h1>

  	<form class="form_edit" action="{{ route('categories.update', $category) }}" method="post" enctype="multipart/form-data">
	@method('PUT')
  	@include('admin.categories.form')

  	</form>

  	<form class="form_delete"  action="{{ route('categories.destroy', $category) }}" method="post">
		@csrf
		@method('DELETE')
		<input class="btn_delete" type="submit" name="submit" value="{{ __('Supprimer la catégorie') }}">
	</form>

</div>
<div class="return">
	<a class="btn" href="{{route('categories.index')}}">retour</a>
</div>
</div>
@endsection

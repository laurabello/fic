@extends('layouts.app')

@section('content')
<div id="ideasByCat">
<h1 class="section_title">Les idées de la categorie {{$category->name}}</h1>
  <catideas :ideas="{{json_encode($ideas)}}"></catideas>
</div>
@endsection

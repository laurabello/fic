@extends('layouts.app')

@section('content')
<div id="ideas">
  <h1 class="section_title">Vos idées</h1>
  <allideas :categories="{{json_encode($categories)}}" :ideas="{{json_encode($ideas)}}"/>
</div>
@endsection

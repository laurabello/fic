@component('mail::message')
# Nouveau Contact

Vous avez reçu un nouveau message de **{{ $contact->contact->firstname }} {{ $contact->contact->lastname }}** ({{ $contact->contact->email }}) le {{ $contact->contact->created_at }} :  
## {{ $contact->contact->title }}   
{{ $contact->contact->message }}  

@endcomponent

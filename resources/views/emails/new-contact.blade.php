@component('mail::message')
# Confirmation de réception de message

Votre message du {{ $contact->contact->created_at }} :
## {{ $contact->contact->title }}   
{{ $contact->contact->message }}

Merci
@endcomponent

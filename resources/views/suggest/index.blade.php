@extends('layouts.app')

@section('content')
  <div id='suggest'>
    @if($req == 'idea')
      <suggest :request='@json($req)' :categories='@json($categories)'/>
    
    @elseif($req == 'initiative')
      @isset($idea)
        @auth
        <suggest :model='@json($idea)' type='idea' :categories='@json($categories)' :request='@json($req)' :user='@json(Auth::user()->load('collectives'))'/>
        @endauth
      @endisset      

      @auth	
      <suggest :categories='@json($categories)' :request='@json($req)' :user='@json(Auth::user()->load('collectives'))'/>
      @endauth

      @guest
      <p>Pour proposer une initiative, vous devez être connecté.</p>
      <a href="{{route('login')}}" title="Aller à la page de connexion">Se connecter - créer un compte</a>
      @endguest
    @endif
  </div>
@endsection

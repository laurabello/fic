@extends('layouts.app')

@section('content')
  {{-- The page banner will have the page image for background --}}
<div class="page_banner" style="background-image:{{$staticPage->picture}};">
    <h1 class="section_title">{{$staticPage->title}}</h1>
  </div>
  <div class="page_wrapper">
    <p class="pageDescription">{{$staticPage->content}}</p>
  </div>
@endsection
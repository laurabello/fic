@extends('layouts.app')
@section('content')
@if(Session::has('message'))
  <p>{{Session::get('message')}}</p>
@endif
<h1 class="section_title">Nous contacter</h1>
<div class="admin_container">
  <div class="infos_contact">
    <h2 class="title">Infos</h2>

      <div>
      <i class="fas fa-map-marked-alt"><p>Adresse:</p></i>
      <address>
        {{$config['address1']['value']}}<br>
        {{$config['address2']['value']}}<br>
        {{$config['zipcode']['value']}}{{$config['city']['value']}}
      </address>
      </div>
      <div>
      <i class="fas fa-at"><p>{{$config['mail']['value']}}</p></i>
      </div>
      <div>
        <i class="fas fa-phone-alt"><p>{{$config['phone']['value']}}</p></i>
      </div>
  </div>

  <div class="form_contact_container">
    <h2 class="title">Directement via le site : </h2>
    <form action="{{route('store_contact')}}" method="post">
      @method('POST')
      @csrf
      <div class="group_field">
        <label for="firstname">Votre prénom :</label>

          <input id="firstname" name="firstname" type="text" required>

      </div>

      <div class="group_field">
        <label for="lastname">Votre nom :</label>
        
          <input id="lastname" name="lastname" type="text">
        
      </div>    

      <div class="group_field">
        <label for="email">Votre email : </label>
        
          <input id="email" name="email" type="email" required>
        
      </div>

      <div class="group_field">
        <label for="phone">Votre téléphone : </label>
        
          <input id="phone" name="phone" type="tel">
        
      </div>

      <div class="group_field">
        <label for="title">Titre :</label>
        
          <input id="title" name="title" type="text" required>
        
      </div>
      
      <div class="contact_textarea">
        <label for="message">Votre message :</label>
        
          <textarea  cols="10" rows="8" id="message" name="message" required></textarea>
        
      </div>

      <div class="form_submit">
        
          <input class="btn" type="submit" value="Envoyer">
        
      </div>
    </form>
  </div>
</div>
@endsection

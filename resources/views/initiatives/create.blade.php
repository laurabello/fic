@extends('layouts.app')

@section('content')
<div>
  <h1>Nouvelle initiative</h1>
  <div>
  	<form action="{{ route('initiatives.store') }}" method="post" class="form">
	@method('POST')
  	@include('initiatives.form')

  	</form>
  </div>
</div>
@endsection

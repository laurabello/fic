@extends('layouts.app')

@section('content')
<div id="initiatives">
  <h1 class="section_title">Vos initiatives</h1>
  <allinitiatives :categories="{{json_encode($categories)}}" :initiatives="{{json_encode($initiatives)}}"></allinitiatives>
</div>
@endsection

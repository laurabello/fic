@extends('layouts.app')

@section('content')
<div>
  <h1>Mettre à jour l'initiative</h1>
  <div>
  	<form action="{{ route('initiatives.update', $initiative) }}" method="put">
      @method('PUT')
      @include('initiatives.form')
    </form>
    <form action="{{ route('initiatives.destroy', $initiative) }}" method="post">
      @csrf
      @method('DELETE')
      <input type="submit" name="submit" value="{{ __('Supprimer l\'initiative') }}">
    </form>
  </div>
</div>
@endsection

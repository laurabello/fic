@extends('layouts.app')
@section('content')

@php
if (Auth::user()) {
  $user = Auth::user();
} else {
  $user = Null;
}
@endphp

<div id="initiative-detail">
  <initiative-detail :initiative="{{ json_encode($initiative) }}" :user="{{ json_encode($user) }}" :similar-inits="{{json_encode($similarInits)}}"/>
@endsection

@extends('layouts.app')

@section('content')
<div id="initiativesByCat">
<h1 class="section_title">Les initiatives de la categorie {{$category->name}}</h1>
  <catinitiatives :initiatives="{{json_encode($initiatives)}}"></catinitiatives>
</div>
@endsection

@csrf
<div>
	<label for="name">{{ __('Nom') }}</label>
	<input id="name" name="name" type="text" value="{{ old('name') ?? $initiative->name }}">
</div>
<div class="form-errors">{{ $errors->first('name') }}</div>
<div>
	<label for="description">{{ __('Description') }}</label>
	<input id="description" name="description" type="text" value="{{ old('description') ?? $initiative->description }}">
</div>
<div class="form-errors">{{ $errors->first('description') }}</div>
<div>
	@if($initiative->id)
	<input class="btn" name="submit" type="submit" value="{{ __('Modifier') }}">
	@else
	<input class="btn" name="submit" type="submit" value="{{ __('Ajouter') }}">
	@endif
</div>
